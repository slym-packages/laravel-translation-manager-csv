<?php

namespace Slym\TranslationManagerCSV;

use Illuminate\Support\Arr;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Foundation\Application;
use Barryvdh\TranslationManager\Models\Translation;

class Manager
{
    /** @var \Illuminate\Contracts\Foundation\Application */
    protected $app;
    /** @var \Illuminate\Filesystem\Filesystem */
    protected $files;

    protected $locales;

    protected $locale;

    protected $ignoreLocales;

    protected $ignoreFilePath;

    public function __construct(Application $app, Filesystem $files)
    {
        $this->app = $app;
        $this->files = $files;
        $this->ignoreFilePath = storage_path('.ignore_locales');
        $this->locales = [];
        $this->locale = config('app.locale');
        $this->ignoreLocales = $this->getIgnoredLocales();
    }

    public function getLocales()
    {
        if (empty($this->locales)) {
            $locales = array_merge([$this->locale],
                Translation::groupBy('locale')->pluck('locale')->toArray());
            foreach ($this->files->directories($this->app->langPath()) as $localeDir) {
                if (($name = $this->files->name($localeDir)) !== 'vendor') {
                    $locales[] = $name;
                }
            }

            $this->locales = array_unique($locales);
            sort($this->locales);
        }

        return array_diff($this->locales, $this->ignoreLocales);
    }

    protected function getIgnoredLocales()
    {
        if (! $this->files->exists($this->ignoreFilePath)) {
            return [];
        }

        $result = json_decode($this->files->get($this->ignoreFilePath), true, 512, JSON_THROW_ON_ERROR);

        return ($result && is_array($result)) ? $result : [];
    }

    public function exportTranslations()
    {
        $translationsGrouped = [];

        Translation::all()
            ->each(function(Translation $translation) use (&$translationsGrouped) {
                if($translation->group === "_json") {
                    $translationsGrouped[$translation->locale][$translation->group][$translation->key] = $translation->value;
                } else {
                    Arr::set($translationsGrouped, $translation->locale . '.' . $translation->group . '.' . $translation->key, $translation->value);
                }
            });

        $locales = $this->getLocales();

        $output = storage_path() . '/translations-untranslated.csv';
        $out = fopen($output, 'wb');

        fputs($out, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

        $row = ['group', 'key'];

        foreach ($locales as $locale) {
            $row[] = $locale;
        }

        fputcsv($out, $row, ';');

        Translation::where('locale', $this->locale)
            ->get()
            ->each(function(Translation $translation) use ($locales, $translationsGrouped, $out) {

                $row = ['group' => $translation->group, 'key' => $translation->key];

                foreach ($locales as $locale) {
                    $row[$locale] = Arr::get($translationsGrouped, $locale . '.' . $translation->group . '.' . $translation->key, '');
                }

                fputcsv($out, $row, ';');
            });

        fclose($out);

        return $output;
    }

    public function importTranslations() {

        $localesCol = [];

        $output = storage_path() . '/translations-translated.csv';

        $row = 1;
        if (($handle = fopen($output, 'rb')) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {
                if($row === 1) {
                    for($i = 2, $iMax = count($data); $i < $iMax; $i++) {
                        $localesCol[$i] = $data[$i];
                    }
                } else {
                    for($i = 2, $iMax = count($data); $i < $iMax; $i++) {
                        if(!empty($data[$i])) {
                            $this->importTranslation($data[1], $data[$i], $localesCol[$i], $data[0]);
                        }
                    }
                }

                $row++;
            }
            fclose($handle);
        }

        return true;
    }

    public function importTranslation($key, $value, $locale, $group)
    {
        if (is_array($value)) {
            return false;
        }

        $value = (string) $value;

        $translation = Translation::firstOrNew([
            'locale' => $locale,
            'group'  => $group,
            'key'    => $key,
        ]);

        // Check if the database is different then the files
        $newStatus = $translation->value === $value ? Translation::STATUS_SAVED : Translation::STATUS_CHANGED;
        if ($newStatus !== (int) $translation->status) {
            $translation->status = $newStatus;
        }

        $translation->value = $value;

        $translation->save();

        return true;
    }

}
