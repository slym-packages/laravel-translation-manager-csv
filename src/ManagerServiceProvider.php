<?php namespace Slym\TranslationManagerCSV;

use Illuminate\Support\ServiceProvider;
use Slym\TranslationManagerCSV\Console\ExportCommand;
use Slym\TranslationManagerCSV\Console\ImportCommand;

class ManagerServiceProvider extends ServiceProvider {

	public function boot()
	{
        if ($this->app->runningInConsole()) {
            $this->commands([
                ExportCommand::class,
                ImportCommand::class,
            ]);
        }
	}
}
