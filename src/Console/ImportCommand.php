<?php

namespace Slym\TranslationManagerCSV\Console;

use Slym\TranslationManagerCSV\Manager;
use Illuminate\Console\Command;

class ImportCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'translations:csv-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import CSV translations to PHP files';

    /** @var \Slym\TranslationManagerCSV\Manager */
    protected $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->manager->importTranslations();

        $this->info('Done ! CSV imported');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
