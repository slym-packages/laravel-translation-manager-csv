<?php

namespace Slym\TranslationManagerCSV\Console;

use Slym\TranslationManagerCSV\Manager;
use Illuminate\Console\Command;

class ExportCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'translations:csv-export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export CSV translations to PHP files';

    /** @var \Slym\TranslationManagerCSV\Manager */
    protected $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $output = $this->manager->exportTranslations();

        $this->info('Done ! CSV created at ' . $output);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
