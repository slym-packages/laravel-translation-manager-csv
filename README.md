## Laravel Translation Manager CSV

This package add command to export/import in CSV at the package [barryvdh/laravel-translation-manager](https://github.com/barryvdh/laravel-translation-manager).

## Installation

Require this package in your composer.json and run composer update (or run `composer require barryvdh/laravel-translation-manager` directly):

    composer require slym/laravel-translation-manager-csv

After updating composer, add the ServiceProvider to the providers array in `config/app.php`

    'Slym\TranslationManagerCSV\ManagerServiceProvider',

## Usage

### Export command

The import command will search through app/lang and load all strings in the database, so you can easily manage them.

    $ php artisan translations:csv-export

Translation strings from db will be exported to storage/translations.csv
